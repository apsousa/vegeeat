package com.example.android.vegeeat.model;

import java.util.ArrayList;

/**
 * Created by asousa on 09-02-2018.
 */

public class Receita {

    private String mNome;
    private ArrayList<String> mIngredientes;
    private String mPreparacao;
    private int mTipoReceita;

    public Receita(String nome, int tipoReceita) {
        mNome = nome;
        mTipoReceita = tipoReceita;
        mIngredientes = new ArrayList<String>();
    }


    public String getNome() {
        return mNome;
    }

    public void setNome(String nome) {
        mNome = nome;
    }

    public ArrayList<String> getIngredientes() {
        return mIngredientes;
    }

    public void setIngredientes(ArrayList<String> ingredientes) {
        mIngredientes = ingredientes;
    }

    public String getPreparacao() {
        return mPreparacao;
    }

    public void setPreparacao(String preparacao) {
        mPreparacao = preparacao;
    }

    public void adicionarIngrediente(String ingrediente) {
        mIngredientes.add(ingrediente);
    }

    public int getTipoReceita() { return mTipoReceita;}

    public String toString() {

        StringBuilder receitasb = new StringBuilder();
        receitasb.append("NOME:");
        receitasb.append(mNome);
        receitasb.append("TIPO DE RECEITA:");
        receitasb.append(mTipoReceita);
        receitasb.append(" - INGREDIENTES:");
        for(String ingrediente: mIngredientes) {
            receitasb.append(ingrediente);
            receitasb.append(" ");
        }

        receitasb.append((" - PREPARAÇÃO: "));
        receitasb.append(mPreparacao);

        return receitasb.toString();
    }

}

