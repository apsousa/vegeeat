package com.example.android.vegeeat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.vegeeat.model.BDReceitas;
import com.example.android.vegeeat.model.Receita;

import java.util.ArrayList;

public class InsertActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private EditText mEtNome, mEtIng1, mEtIng2, mEtIng3, mEtIng4, mEtPreparacao;
    private Spinner mSpTipoPrato;
    private Receita mReceita;
    private ArrayList<Receita> mReceitas;
    int mTipoPrato;
    private Button mButton;
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        Intent intent = getIntent();
        int numreceita = intent.getIntExtra("NUMRECEITA",-1);
        /*
        Se numreceita = 0: inserir uma nova receita,
        caso contrário mostrar a receita indicada em numreceita
         */

        mTextView = (TextView)findViewById(R.id.tv_titulo_operacao);
        if( numreceita >= 0 ) {
            mTextView.setText("Consultar Receita");
            // obter receita
            mReceita = ((BDReceitas) getApplication()).getReceitas().get(numreceita);
        }



        mEtNome = (EditText) findViewById(R.id.et_nome);
        if( numreceita >= 0) {
            mEtNome.setText(mReceita.getNome());
         }

        mEtIng1 = (EditText) findViewById(R.id.et_ingrediente1);
        mEtIng2 = (EditText) findViewById(R.id.et_ingrediente2);
        mEtIng3 = (EditText) findViewById(R.id.et_ingrediente3);
        mEtIng4 = (EditText) findViewById(R.id.et_ingrediente4);
        if( numreceita >= 0) {
            ArrayList<String> ingredientes = mReceita.getIngredientes();
            // tosco mais dá para os gastos
            int i = 1;
            for (String ingrediente:ingredientes ) {
                switch(i) {
                    case 1:
                        mEtIng1.setText(ingrediente);
                         break;
                    case 2:
                        mEtIng2.setText(ingrediente);
                         break;
                    case 3:
                        mEtIng3.setText(ingrediente);
                         break;
                    case 4:
                        mEtIng4.setText(ingrediente);
                         break;
                }
                i++;
            }
        }

        mEtPreparacao = (EditText) findViewById(R.id.et_preparacao);
        if( numreceita >= 0) {
            mEtPreparacao.setText(mReceita.getPreparacao());
         }

        mSpTipoPrato = (Spinner) findViewById(R.id.sp_tipoprato);

        BDReceitas receitas = (BDReceitas) getApplication();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, receitas.getTipoReceitas());

        mSpTipoPrato.setAdapter(adapter);
        mSpTipoPrato.setOnItemSelectedListener(this);
        if( numreceita >= 0) {
            mSpTipoPrato.setSelection(mReceita.getTipoReceita());
         }

        mButton = (Button) findViewById(R.id.bt_gravar);
        if(numreceita >= 0 ) {
            mButton.setVisibility(View.INVISIBLE);
        }

    }

    public void gravar(View v) {

        // Obter dados
        String nome = (mEtNome.getText().toString()).trim();
        String ing1 = (mEtIng1.getText().toString()).trim();
        String ing2 = (mEtIng2.getText().toString()).trim();
        String ing3 = (mEtIng3.getText().toString()).trim();
        String ing4 = (mEtIng4.getText().toString()).trim();
        String preparacao = (mEtPreparacao.getText().toString()).trim();

        // Validar dados
        if( nome.equals("")) {
            Toast.makeText(this,"Preencha o nome da receita.", Toast.LENGTH_SHORT).show();
            mEtNome.requestFocus();
            return;
        }

        if( mTipoPrato == 0) {
            Toast.makeText(this,"Selecione o tipo de prato.", Toast.LENGTH_SHORT).show();
            mSpTipoPrato.requestFocus();
            return;
        }

        if( ing1.equals("") && ing2.equals("") & ing3.equals("") && ing4.equals("")) {
            Toast.makeText(this,"Indique pelo menos um ingrediente.", Toast.LENGTH_SHORT).show();
            mEtIng1.requestFocus();
            return;
        }

        if( preparacao.equals("")) {
            Toast.makeText(this,"Indique os passos da preparação.", Toast.LENGTH_SHORT).show();
            mEtPreparacao.requestFocus();
            return;
        }


        // Dados validados, podemos criar a receita e guardá-la
        Receita receita = new Receita(nome, mTipoPrato);
        receita.setPreparacao(preparacao);

        if(!ing1.equals("")) {
            receita.adicionarIngrediente(ing1);
        }

        if(!ing2.equals("")) {
            receita.adicionarIngrediente(ing2);
        }

        if(!ing3.equals("")) {
            receita.adicionarIngrediente(ing3);
        }

        if(!ing4.equals("")) {
            receita.adicionarIngrediente(ing4);
        }

        mReceitas = ((BDReceitas) getApplication()).getReceitas();
        mReceitas.add(receita);

        // Mostrar mensagem
        Toast.makeText(this, "Criada nova receita", Toast.LENGTH_SHORT).show();

        // Limpar formulário
        limpaFormulario();

    }

    private void limpaFormulario() {

        mEtPreparacao.setText("");

        mEtIng1.setText("");
        mEtIng2.setText("");
        mEtIng3.setText("");
        mEtIng4.setText("");
        mEtNome.setText("");
        mSpTipoPrato.setSelection(0);
        mEtNome.requestFocus();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        mTipoPrato = i;
        Toast.makeText(this, "Tipo de prato selecionado: " + adapterView.getItemAtPosition(i), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
