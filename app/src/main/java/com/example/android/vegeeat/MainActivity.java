package com.example.android.vegeeat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.android.vegeeat.model.BDReceitas;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void processaMenu(View v) {
        Intent intent;
        switch (v.getId()) {

            case R.id.ib_novareceita:
                intent = new Intent(this, InsertActivity.class);
                intent.putExtra("NUMRECEITA", -1); // Inserir nova receita
                startActivity(intent);
                break;

            case R.id.ib_listarreceitas:
                intent = new Intent(this, ListarActivity.class);
                startActivity(intent);
                break;

            case R.id.ib_receitasaoacaso:
                intent = new Intent(this, InsertActivity.class);

                int superior = ((BDReceitas) getApplication()).getReceitas().size();
                if( superior == 0 ) {
                    Toast.makeText(this, "Insira pelo menos 1 receita!", Toast.LENGTH_SHORT).show();
                    return;
                }
                int inferior = 0;
                Random r = new Random();
                int numreceita = r.nextInt(superior-inferior) + inferior;
                intent.putExtra("NUMRECEITA", numreceita); // MOSTRAR RECEITA número numreceita
                startActivity(intent);
                break;

        }
    }
}
