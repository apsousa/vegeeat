package com.example.android.vegeeat;

import android.content.DialogInterface;
import android.os.PersistableBundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.android.vegeeat.model.BDReceitas;
import com.example.android.vegeeat.model.Receita;

import java.util.ArrayList;
import java.util.Arrays;

public class ListarActivity extends AppCompatActivity {

    private final String TAG = "listar_activity";

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ReceitasAdapter mAdapter;

    private int filtroAtivo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);

        // No início mostra todos os pratos
        filtroAtivo = 0;
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_receitas);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        ArrayList<Receita> receitas = obterReceitas(filtroAtivo);

        mAdapter = new ReceitasAdapter(this,receitas);
        mRecyclerView.setAdapter(mAdapter);

    }


    private ArrayList<Receita> obterReceitas(int filtroAtivo) {

        ArrayList<Receita> receitas;

        if( filtroAtivo == 0 ) {
            receitas = new ArrayList<>(((BDReceitas) getApplication()).getReceitas());
        } else {
            receitas = new ArrayList<>();
            for( Receita receita: ((BDReceitas) getApplication()).getReceitas()) {
                if( receita.getTipoReceita() == filtroAtivo) {
                    receitas.add(receita);
                }
            }
        }

        return receitas;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.filtro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_item_filtro:
                Log.i(TAG, "CLICADO FILTRO");
                DialogFilter();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void DialogFilter() {
         AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Obter tipos de prato guardando-os numa array do tipo String
        String tiposPratoTodos[] = (((BDReceitas) getApplication()).getTipoReceitas()).toArray(new String[0]);
        tiposPratoTodos[0] = "Todos";
        // Preciso retirar o primeiro elemento que é selecione...
        // String tiposPrato[] = Arrays.copyOfRange(tiposPratoTodos, 1, tiposPratoTodos.length-1);

        builder.setTitle("Filtro ativo: " + tiposPratoTodos[filtroAtivo]);

        builder.setItems(tiposPratoTodos, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if( filtroAtivo != which )  {
                            filtroAtivo = which;
                            // obter receitas que satisfaçam o filtro
                            ArrayList<Receita> receitas = obterReceitas(filtroAtivo);
                            // Forçar atualização da recyclerview
                            mRecyclerView.setAdapter(null);
                            mRecyclerView.setLayoutManager(null);
                            mRecyclerView.setAdapter(mAdapter);
                            mAdapter.setReceitas(receitas);
                            mRecyclerView.setLayoutManager(mLayoutManager);
                            mAdapter.notifyDataSetChanged();
                        }

                    }
                });

        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        AlertDialog dialog = builder.create();

        dialog.show();
    }

}
