package com.example.android.vegeeat;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.vegeeat.model.BDReceitas;
import com.example.android.vegeeat.model.Receita;

import java.util.ArrayList;

/**
 * Created by asousa on 27-02-2018.
 */

    public class ReceitasAdapter extends  RecyclerView.Adapter<com.example.android.vegeeat.ReceitasAdapter.ReceitaHolder> {

        private ArrayList<Receita> mReceitas;
        private Context mContext;
        private Toast mToast;


        public ReceitasAdapter(Context context, ArrayList<Receita> receitas) {
            mReceitas = receitas;
        }

        public void setReceitas(ArrayList<Receita> receitas) {
            mReceitas = receitas;
        }

        @Override
        public ReceitaHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_item, parent, false);

            ReceitaHolder vh = new ReceitaHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ReceitaHolder holder, int position) {
            //holder.nomeReceita.setText(mContatos.get(position));
            holder.nomeReceita.setText(mReceitas.get(position).getNome().toString());


        }

        @Override
        public int getItemCount() {
            return mReceitas.size();
        }


        /*
        --------- VIEW HOLDER -----------
         */
        public class ReceitaHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            public TextView nomeReceita;

            public ReceitaHolder(View itemView) {
                super(itemView);
                nomeReceita = itemView.findViewById(R.id.tv_nome_receita);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {

                if( mToast != null ) {
                    mToast.cancel();
                }
                mToast = Toast.makeText(mContext,"Receita clicada: " + getAdapterPosition(), Toast.LENGTH_SHORT);
                mToast.show();

                Intent intent = new Intent(mContext, InsertActivity.class);
                intent.putExtra("NUMRECEITA", getAdapterPosition()); // MOSTRAR RECEITA número numreceita
                mContext.startActivity(intent);

            }
        }

    /*
    ----------------------------------
     */


    }




