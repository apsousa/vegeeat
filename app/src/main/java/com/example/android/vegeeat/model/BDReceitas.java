package com.example.android.vegeeat.model;

import android.app.Application;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by asousa on 27-02-2018.
 */

public class BDReceitas extends Application {

    private ArrayList<Receita> mReceitas;
    private ArrayList<String> mTipoReceitas;

    @Override
    public void onCreate() {
        super.onCreate();

        mReceitas = new ArrayList<>();

        Receita receita = new Receita("R1-Entrada", 1);
        receita.adicionarIngrediente("ingrediente1");
        receita.adicionarIngrediente("ingrediente2");
        receita.adicionarIngrediente("ingrediente3");
        receita.setPreparacao("Juntar todos 3 e mexer");
        mReceitas.add(receita);

        receita = new Receita("R2-PratoPrin", 2);
        receita.adicionarIngrediente("ingrediente21");
        receita.adicionarIngrediente("ingrediente22");
        receita.adicionarIngrediente("ingrediente23");
        receita.setPreparacao("Juntar todos 3 e misturar");
        mReceitas.add(receita);

        receita = new Receita("R3-Sanduba", 3);
        receita.adicionarIngrediente("ingrediente31");
        receita.adicionarIngrediente("ingrediente32");
        receita.adicionarIngrediente("ingrediente33");
        receita.setPreparacao("Juntar todos 3 e colocar em banho maria");
        mReceitas.add(receita);

        receita = new Receita("R4-Sanduba", 3);
        receita.adicionarIngrediente("ingrediente41");
        receita.adicionarIngrediente("ingrediente42");
        receita.adicionarIngrediente("ingrediente43");
        receita.adicionarIngrediente("ingrediente44");
        receita.setPreparacao("Juntar todos 4 e coar");
        mReceitas.add(receita);

        mTipoReceitas = new ArrayList<>();
        mTipoReceitas.add("Selecione...");
        mTipoReceitas.add("Entradas");
        mTipoReceitas.add("Pratos principais");
        mTipoReceitas.add("Sanduiches");
        mTipoReceitas.add("Sobremesas");
        mTipoReceitas.add("Sopas");


    }


    public ArrayList<Receita> getReceitas() {
        return mReceitas;
    }

    public ArrayList<String> getTipoReceitas() {
        return mTipoReceitas;
    }
}
